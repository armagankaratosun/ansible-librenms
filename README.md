# Ansible Role: LibreNMS Setup

Ansible role for making required configurations on pre-build LibreNMS image.

## Requirements

* Ansible (really?)
* LibreNMS Pre-build Setup
* Some crazy CLI skills

## Role Variables

* Check `vars:` in `main.yml`

## Supported Platforms

* [Pre-build LibreNMS Ubuntu 16.04 image](https://docs.librenms.org/#Installation/Ubuntu-image/)

## Example Hosts File

``` bash
[librenms-pollers]
your-poller-ips secret-passwd.
```

## Tasks

 - [x] OS Update
 - [x] Set Timezone (systemd)
 - [x] Set Locale (systemd)
 - [x] Set NTP
 - [x] Set system defaults
 - [x] Set rc.local
 - [x] Deploy and configure audit
 - [x] Deploy and configure vnstat
 - [x] Deploy and configure monit

## TO DO

 - [ ] Poller-onyl settings
 - [ ] Set ufw
 - [ ] mysql backup
